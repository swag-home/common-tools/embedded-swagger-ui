# 🔬 Embedded Swagger UI

This project is an Arduino / PlatformIO (using Arduino framework) example implementation to expose a Swagger UI interface on an ESP mcu for a REST API to control a device

## Why this project?

Because IoT would generate much less e-waste and devices would have a longer life, be maintanable and more interoperable if they'd
simply expose their documentation and API instead of always requiring reverse-engineering every time support stops or company
goes out of business.

## How to use it?

Once you installed the library by cloning this folder in your `~Documents/Arduino/libraries` folder just include the `SwaggerUI.h` file in your project and call the `setup_swagger_ui` function in your setup context.

You can base yourself on the basic example included with the library 😉

Or if you want to expose it yourself on any other MCU or any other media than internal program flash (through a virtual FS or an SD card for example) feel free to reuse compressed assets and expose your SwaggerUI, it's easy really

## Related documentation

You can find more documentation about the reason on creating this simple example project in the subgroup [Documentation](https://gitlab.com/swag-home/documentation) of the Gitlab group and more implementation examples in other subgroups.

Root of the group [Swag@Home can be found here](https://gitlab.com/swag-home/documentation)
