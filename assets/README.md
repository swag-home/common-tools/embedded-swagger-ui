# EXAMPLE OPENAPI SPECIFICATION

This folder contains an example / minimalistic openapi specification for the REST API of an IoT device targeting reusability

## How to generate bytes array to expose the documentation?

To generate the bytes array containing your compressed specification to expose on your device, follow these steps:

1) Convert your `openapi.yaml` specification file to `swagger.json` variant, for example by using Swagger Editor online
2) Minimy your `swagger.json` to reduce extra indentation and whitespaces -> you can easily use online tools
3) Compress your `swagger.json` file by executing the following command: `cat swagger.json | gzip -9 > swagger.json.gz`
4) Convert your gzip file to C array using `bin2c swagger.json.gz` command (you can download bin2c online)
