#ifndef SWAGGERUI_H
#define SWAGGERUI_H

#include <Arduino.h>
#include <stdint.h>

#ifdef ESP8266
#include <ESP8266WebServer.h>

using EmbeddedWebServer = ESP8266WebServer;
#elif defined(ESP32)
#include <WebServer.h>

using EmbeddedWebServer = WebServer;
#endif // ESP8266 || ESP32

typedef struct OpenApiSpec {
    PGM_P specificationFile;
    size_t specificationFileLength;
} OpenApiSpec;

void setup_swagger_ui(EmbeddedWebServer & httpServer, const OpenApiSpec & open_api_spec);

extern const char _acindex[1431UL + 1] PROGMEM;
extern const char _acswagger_ui_bundle_js[333430UL + 1] PROGMEM;
extern const char _acswagger_ui_standalone_preset_js[103710UL + 1] PROGMEM;
extern const char _acswagger_ui[21795UL + 1] PROGMEM;

#endif // SWAGGERUI_H
