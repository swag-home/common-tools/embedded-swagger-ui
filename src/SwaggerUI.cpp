#include "SwaggerUI.h"

void setup_swagger_ui(EmbeddedWebServer & httpServer, const OpenApiSpec & openApiSpec)
{
    httpServer.on("/swagger/index.html", [&]() {
      httpServer.send(200, F("text/html"), FPSTR(_acindex));
    });
    httpServer.on("/swagger/swagger.json", [&]() {
      httpServer.sendHeader(F("Content-Encoding"), F("gzip"));
      httpServer.send_P(200, PSTR("application/json"), openApiSpec.specificationFile, openApiSpec.specificationFileLength);
    });
    httpServer.on("/swagger/swagger-ui.css", [&]() {
      httpServer.sendHeader(F("Content-Encoding"), F("gzip"));
      httpServer.send_P(200, PSTR("text/css"), _acswagger_ui, 21795UL);
    });
    httpServer.on("/swagger/swagger-ui-bundle.js", [&]() {
      httpServer.sendHeader(F("Content-Encoding"), F("gzip"));
      httpServer.send_P(200, PSTR("text/javascript"), _acswagger_ui_bundle_js, 333430UL);
    });
    httpServer.on("/swagger/swagger-ui-standalone-preset.js", [&]() {
      httpServer.sendHeader(F("Content-Encoding"), F("gzip"));
      httpServer.send_P(200, PSTR("text/javascript"), _acswagger_ui_standalone_preset_js, 143554UL);
    });
}
